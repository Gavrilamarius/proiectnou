package com.sda.marius.Utils;

import java.util.Scanner;

public class ScannerUtils {

    private static Scanner scanner;

    public static Scanner getScanner() {
        if(scanner==null) {
            scanner = new Scanner(System.in);
        }
        return scanner;
    }

    public static int getNumberFromConsole() {
       Scanner scanner = getScanner();
       int userNumberInput;
       try{
           userNumberInput = scanner.nextInt();
       }catch(Exception exception){
           System.out.println("Introdu un numar valid");
           scanner.nextLine();
           userNumberInput = getNumberFromConsole();
       }
       return userNumberInput;
    }


    public static float getNumberFromConsoleForFloat() {
        Scanner scanner = getScanner();
        float userNumberInput;
        try{
            userNumberInput = scanner.nextFloat();
        }catch(Exception exception){
            System.out.println("Introdu un numar valid");
            scanner.nextLine();
            userNumberInput = getNumberFromConsole();
        }
        return userNumberInput;
    }


    public static float PerimetruCircle(){
        Scanner scanner = getScanner();
        int raza = scanner.nextInt();
        float perimetru;
        perimetru = 2 * 3.14f * raza;
        return perimetru;
    }


    public static void fizzBuzz() {
        System.out.println("Introdu numarul de la tastura");
        int numar = ScannerUtils.getNumberFromConsole();
        for (int i = 1; i <= numar; i++) {
            System.out.println(i);
            if (i % 3 == 0) {
                System.out.println(i + "Fizz");
            } else if (i % 7 == 0) {
                System.out.println(i + "Buzz");
            }
             else if (i % 7 == 0 && i % 3 == 0) {
                System.out.println(i + "FizzBuzz");
            } else if (i % 2 == 0) {
                System.out.println(i + "Divizbil cu doi");
            }
        }
    }
}
