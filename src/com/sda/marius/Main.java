package com.sda.marius;

public class Main {

    public static void main(String[] args) {
        System.out.printf("Numele este %s si prenumele este %s", "Gavrila", "Marius");
        System.out.println();
        addName("Gavrila", "Marius");
        System.out.println();
        printTable();
        numberWithTwoDecimals(1.455d);
        System.out.println();
        triangle(10);
    }


    public static void addName(String firstName, String lastName) {
        System.out.printf("Numele este %10s si prenumele este %-10s" , lastName, firstName);
    }


    public static void printTable(){
        System.out.printf("%s %15s\n", "Exam name", "Exam grade");
        System.out.println("---------------------------------");
        System.out.printf("%-18s %s\n", "Java", "A");
        System.out.printf("%-18s %s\n", "Net.", "B");
        System.out.printf("%-18s %s\n", "PHP", "C");
        System.out.println("---------------------------------");
    }

    public static void numberWithTwoDecimals(double numberInput){
        System.out.println(String.format("%.2f", numberInput));
    }



    public static void triangle(int N){
        for(int i = 1; i<N ; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j);
            }

            System.out.println();
        }
    }
}
