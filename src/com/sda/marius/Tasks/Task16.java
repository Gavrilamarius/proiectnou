package com.sda.marius.Tasks;

import java.util.Arrays;
import java.util.Scanner;

public class Task16 {
    public static void main(String[] args) {
        distanceBetweenNumbers(2,10);
    }


    public static void distanceBetweenNumbers(int firstPosition, int secondPosition) {
        int counter = 0;
        System.out.println("Insert dimension for array : ");
        Scanner A = new Scanner(System.in);
        int n = A.nextInt();
        int[] arrayListOfNumber = new int[n];
        for (int i = 0; i <= n; i++) {
            System.out.print("arrayListOfNumber[" + i + "]= ");
            arrayListOfNumber[i] = A.nextInt();
        }
        System.out.println(" Sirul introdus este " + Arrays.toString(arrayListOfNumber));

        for (int i = arrayListOfNumber[firstPosition]; i <= arrayListOfNumber[secondPosition]; i++) {
            counter++;
        }
        System.out.println("Distanta dintre cele doua numere este " + counter);
    }
}
