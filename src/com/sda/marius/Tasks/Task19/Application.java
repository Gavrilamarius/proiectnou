package com.sda.marius.Tasks.Task19;

public class Application {
    private Author[] authors;
    private Poem[] poems;
    public static int position = 0;

    Application(){
        this.authors = new Author[10];
        this.poems= new Poem[10];
        position = 0;
    }

    public void addList(Poem poems) {
        this.poems[position] = poems;
        position++;
    }

    public void longestPoem() {
        int max = 0;
        for (Poem poem : poems) {
            if (poem != null && poem.getStropheNumber() > max) {
                max = poem.getStropheNumber();
                System.out.println("Numarul de strofe cel maim are este " + max);
            }
            if(poem!=null && max == poem.getStropheNumber()){
                System.out.println("Autorul este " + poem.getCreator() + " " + "si a scris poezia " + poem.getTitle());
            }
        }
    }

    public void showList(){
        for(Poem poem : poems){
            if(poem!=null ){
                System.out.println(poem);
            }
        }
    }

}
