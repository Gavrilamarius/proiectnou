package com.sda.marius.Tasks.Task19;

public class Main {
    public static void main(String[] args) {
        Poem luceafarul = new Poem(new Author("Mihai", "Eminescu", "roman"), 98, "Luceafarul");
        Poem floareAlbastra = new Poem(new Author("Mihai", "Eminescu", "roman"), 14, "Floare Albastra");
        Poem plumb = new Poem(new Author("George", "Bacovia", "roman"), 2, "Plumb");
        Application A = new Application();
        A.addList(luceafarul);
        A.addList(floareAlbastra);
        A.addList(plumb);
        A.showList();
        A.longestPoem();
    }
}
