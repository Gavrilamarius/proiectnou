package com.sda.marius.Tasks.Task19;

public class Poem {
    private Author creator;
    private int stropheNumber;
    private String title;

    Poem() {
    }

    public Poem(Author creator, int stropheNumber) {
        this.creator= creator;
        this.stropheNumber = stropheNumber;
    }

    public Poem(Author creator, int stropheNumber, String title) {
        this.creator = creator;
        this.stropheNumber = stropheNumber;
        this.title = title;
    }

    public Author getCreator() {
        return creator;
    }


    public int getStropheNumber() {
        return stropheNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCreator(Author creator) {
        this.creator = creator;
    }

    public void setStropheNumber(int stropheNumber) {
        this.stropheNumber = stropheNumber;
    }


    @Override
    public String toString() {
        return "Poem{" +
                "creator=" + creator +
                ", stropheNumber=" + stropheNumber +
                ", title='" + title + '\'' +
                '}';
    }
}

