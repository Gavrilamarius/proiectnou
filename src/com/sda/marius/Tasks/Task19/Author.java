package com.sda.marius.Tasks.Task19;

public class Author {
    private String firstName;
    private String lastName;
    private String nationality;


    Author() {

    }

    Author(String firstname) {
        this.firstName = firstName;
    }

    Author(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    Author(String firstName, String lastName, String nationality){
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationality = nationality;
    }

    //GETTERS;

    public String getFirstName(){
        return this.firstName;
    }

    public String getLastName(){
        return this.lastName;
    }

    public String getNationality(){
        return this.nationality;
    }

    //SETTERS;

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public void setLastName(String lastName){
        this.lastName = lastName;
    }

    public void setNationality(String nationality){
        this.nationality = nationality;
    }

    public String toString(){
        return "Author with " + this.firstName + " " + this.lastName + " with nationality " +  this.nationality;
    }

}
