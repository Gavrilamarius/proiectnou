package com.sda.marius.Tasks;

import static java.lang.Math.sqrt;

public class Task3 {
    public static void main(String[] args) {
        delta(1,-5,4);

    }

    public static int delta(int a, int b, int c){
        int delta = 0;
        delta = (b*b) - (4*a*c);
        if(delta < 0){
            System.out.println("Delta is negative");
            return delta;}
        else{
            int x1;
            int x2;
            x1 = ((int) ((-b) - sqrt(delta)))/(2*a);
            x2 = ((int) ((-b) + sqrt(delta)))/(2*a);
            System.out.println("X1 is equal with " + x1);
            System.out.println("X2 is equal with " + x2);
        }
        return delta;
    }
}
