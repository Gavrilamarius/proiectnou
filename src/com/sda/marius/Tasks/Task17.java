package com.sda.marius.Tasks;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Task17 {
    public static void main(String[] args) {
        countDays();
    }

    public static void countDays() {
        LocalDate a = LocalDate.now();
        System.out.println("Data este curenta " + a.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        LocalDate b = LocalDate.of(2022, 4, 9);
        System.out.println("Data cursului este " + b.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
        Period d = Period.between(a, b);
        System.out.println("Tu mai ai " + d + " zile pana la curs, EXERSEAZA in continuare");

    }
}


