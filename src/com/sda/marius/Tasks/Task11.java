package com.sda.marius.Tasks;

import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        textTest();

    }

    public static void textTest() {
        System.out.println("Insert your text");
        Scanner a = new Scanner(System.in);
        String text = a.nextLine();
        int limitForText = 10;
        if (text.length() >= 1 && text.length() <= limitForText) {
            System.out.println(text + " - > Enough");

        } else if (text.length() > limitForText) {
            System.out.println(text + " - > Over text");
        } else if (text.isEmpty()) {
            System.out.println("Not enough");
        }

    }
}
