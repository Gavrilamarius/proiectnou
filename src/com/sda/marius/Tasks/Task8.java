package com.sda.marius.Tasks;

import java.util.Scanner;

public class Task8 {
    public static void main(String[] args) {
        resultOfOperation();

    }

    public static float resultOfOperation() {
        float result = 0f;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Insert first number");
        float numberOne = scanner.nextInt();
        System.out.println("Insert operation mathematical / * + -");
        String symbol = scanner.next();
        System.out.println("Insert second number");
        float numberTwo = scanner.nextInt();
        switch (symbol) {
            case "/":
                result = (numberOne / numberTwo);
                System.out.println("Result for division is " + result);
                break;
            case "*":
                result = (numberOne * numberTwo);
                System.out.println("Result for multiplication is " + result);
//                return result;
                break;
            case "+":
                result = (numberOne + numberTwo);
                System.out.println("Result for assembly is " + result);
                return result;
//                break;
            case "-":
                result = (numberOne - numberTwo);
                System.out.println("Result for differences is " + result);
                return result;
//                break;
            default:
                System.out.println("Invalid symbol");
                System.out.println("Cannot calculate");
                break;
        }
        return result;
    }
}
