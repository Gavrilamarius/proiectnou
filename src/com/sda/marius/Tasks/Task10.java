package com.sda.marius.Tasks;

import java.security.spec.RSAOtherPrimeInfo;
import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        System.out.println("Insert your number,please : ");
        Scanner A = new Scanner(System.in);
        int number = A.nextInt();
        sumOfDigits(number);

    }

    public static int sumOfDigits(int number) {
        int sum = 0;
        while(number!=0) {
            sum = sum + (number % 10);
            number = number / 10;
        }
        System.out.println("Suma este " + sum);
        return sum;
    }
}
