package com.sda.marius.Tasks;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task12 {
    public static void main(String[] args) {
        System.out.println("Insert your text please");
        Scanner a = new Scanner(System.in);
        String textInput = a.nextLine();
        countSpace(textInput);
    }

    public static void countSpace(String textInput) {
        int counter = 0;
        Pattern pattern = Pattern.compile(" ");
        Matcher matcher = pattern.matcher(textInput);
        while (matcher.find()) {
            counter++;
        }
        if (counter > 0) {
            System.out.println("Number of space is " + counter);
        } else {
            System.out.println("Doesn't exist no one space ");
        }
    }
}