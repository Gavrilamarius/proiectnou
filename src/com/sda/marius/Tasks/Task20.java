package com.sda.marius.Tasks;

import java.util.Scanner;

public class Task20 {
    public static void main(String[] args) {
        tooMuchTooLittle();
    }


    public static void tooMuchTooLittle() {
        int numberComputer;
        numberComputer = (int) (Math.random() + 10);
        Scanner A = new Scanner(System.in);
        System.out.println("Jocul a inceput");
        System.out.println("INFO: DACA GHICESTI NUMELE AI CASTIGAT");
        System.out.println("NUMARUL DE INCERCARI ESTE [3]");
        System.out.println("Introdu numarul te rog");
        int numberUser = A.nextInt();
        int numberForTry = 3;
        int numberUser1 = numberUser;
        if (numberUser1 != numberComputer) {
            do {
                numberForTry--;
                System.out.println("Mai ai " + numberForTry + " de incercari");
                System.out.println("Introdu numarul din nou");
                numberUser1 = A.nextInt();
            }
            while (numberUser1 != numberComputer && numberForTry != 0);
        }

        if (numberForTry == 0) {
            System.out.println("Nu mai ai incercari! ");
            System.out.println("Ai pierdut");
        }

        if (numberUser1 == numberComputer) {
            System.out.println("Congralations");
        }
    }
}
