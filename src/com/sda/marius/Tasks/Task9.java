package com.sda.marius.Tasks;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        System.out.println("Insert your number for wave, please");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        printWave(number);

    }

    public static void printWave(int number) {
        for (int i = 0; i <= number; i++) {
            for (int j = 0; j <= number * number * 2; j++) {
                if (j % (number * 2) == number - 1 || j % (number * 2) == number + i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}


