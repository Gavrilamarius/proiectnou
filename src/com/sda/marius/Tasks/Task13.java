package com.sda.marius.Tasks;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Task13 {
    public static void main(String[] args) {
        methodOneForSttuters();
        methodTwoForSttuters();
        Scanner B = new Scanner(System.in);
        methodThreeWithVarArg("This" , "is" , "my" , "test");


    }

    public static void methodOneForSttuters() {
        System.out.println("Insert numarul de cuvinte din sir");
        Scanner A = new Scanner(System.in);
        int N = A.nextInt();
        String[] input = new String[N];
        for (int i = 0; i < N; i++) {
            input[i] = A.next();
        }
        System.out.println("New text is : \n ");
        for (String word : input) {
            System.out.print(word.toUpperCase(Locale.ROOT) + " " + word + " ");
        }
    }

    public static void methodTwoForSttuters() {
        System.out.println("Insert your text");
        Scanner A = new Scanner(System.in);
        String[] input = new String[]{A.next(), A.next(), A.next()};
        System.out.println(Arrays.deepToString(input));
        for (String word : input) {
            System.out.print(word + " " + word + " ");
            continue;
        }
    }

    public static void methodThreeWithVarArg(String ... words){
        for(String word : words){
            System.out.println(word.toLowerCase(Locale.ROOT) + " " + word + " ");
        }
    }

}