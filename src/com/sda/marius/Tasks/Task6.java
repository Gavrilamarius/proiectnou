package com.sda.marius.Tasks;

import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        System.out.println("Insert your number");
        Scanner A = new Scanner(System.in);
        int n = A.nextInt();
        sumOfHarmonic(n);

    }

    public static float sumOfHarmonic(int n) {
        int suma = 0;
        for (int i = 1; i <= n; i++) {
             float harmonic = 1/i;
             suma +=  harmonic;
        }
        System.out.println("Suma este " + suma);
        return suma;
    }
}
