package com.sda.marius.Tasks;

import java.util.Scanner;

public class Spanzatoarea {
    private static String[] words = {"casa", "animal", "papagal", "decebal"};
    private static String word = words[(int) (Math.random() * words.length)];
    private static String incognito = new String(new char[word.length()]).replace("\0", "*");
    private static int count = 0;

    public static void main(String[] args) {
        Scanner A = new Scanner(System.in);
        while (count < 7 && incognito.contains("*")) {
            System.out.println("Ghiceste orice litera din acest cuvant");
            System.out.println(incognito);
            String myGuess = A.next();
            hangMan(myGuess);
        }
    }

    public static void hangMan(String myGuess) {
        String newDate = "";
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == myGuess.charAt(0)) {
                newDate += myGuess.charAt(0);
            } else if (incognito.charAt(i) != '*') {
                newDate += word.charAt(i);
            } else {
                newDate +=  "*";
            }
        }

        if (incognito.equals(newDate)) {
            count++;
        } else {
            incognito = newDate;
        }
        if (incognito.equals(word)) {
            System.out.println("Ai castigat, cuvantul a fost " + word);
        }
    }
}
