package com.sda.marius.Tasks;

import java.util.Scanner;

public class Task15 {
    public static void main(String[] args) {
        numberOccuredTwice();
    }

    public static int[] readMyArray() {
        System.out.println("Introdu te rog dimensiunea sirului");
        Scanner A = new Scanner(System.in);
        int N = A.nextInt();
        int[] numbers = new int[N];
        for (int i = 0; i < N; i++) {
            System.out.println("number[" + i + "]=");
            numbers[i] = A.nextInt();
        }
        return numbers;
    }

    public static void numberOccuredTwice(){
        int[] numbers = readMyArray();
        for (int i = 0; i < numbers.length; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                if (numbers[i] == numbers[j]) {
                    System.out.println("Numarul duplicat este " + numbers[j]);
                }
            }
        }
    }
}

