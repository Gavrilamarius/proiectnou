package com.sda.marius.Tasks;

public class Task2 {
    public static void main(String[] args) {
        bmiIndex(36.5F, 1.80F);

    }

    public static float bmiIndex(float weight, float height) {
        float bmi;
        bmi = weight / height;
        System.out.println("BMIcalculate have result " + bmi);
        if (bmi > 18.9 && bmi < 24.9) {
            System.out.println("BMI not optimal");
        } else {
            System.out.println("BMI is optimal");
        }
        return bmi;
    }
}
