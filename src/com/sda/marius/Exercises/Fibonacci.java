package com.sda.marius.Exercises;

public class Fibonacci {
    public static void main(String[] args) {
        Fibonacci apel = new Fibonacci();
        System.out.println(apel.CalculateeFibonacciNummber(0));

    }

     public int CalculateeFibonacciNummber(int position){
        if(position == 0 || position == 1){
            return 1;
        }
        int number1 = 1;
        int number2 = 1;
        int result=0;
        for(int i = 2; i<=position; i++){
            result = number1+number2;
            number1 = number2;
            number2 = result;}
        return result;
     }
}
